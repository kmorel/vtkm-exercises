#include <vtkm/io/reader/VTKDataSetReader.h>

#include <vtkm/filter/PointElevation.h>

#include <vtkm/rendering/Actor.h>
#include <vtkm/rendering/CanvasRayTracer.h>
#include <vtkm/rendering/MapperRayTracer.h>
#include <vtkm/rendering/Scene.h>
#include <vtkm/rendering/View3D.h>

// For this exercise we are going to read a legacy VTK file containing
// polygons. (You can select the file as a command line option, but the
// cow.vtk file provided works well.) We then run the elevation filter on
// the data and render the data.
void Exercise(const char *filename)
{
  // Step 1
  // Create a vtkm::io::reader::VTKDataSetReader object. The constructor for
  // VTKDataSetReader takes the filename.

  // Step 2
  // Use the reader object to load the data from the file. This is done by
  // calling the ReadDataSet method. The resulting data is returned in a
  // vtkm::cont::DataSet object.

  // Step 3
  // Before we move on to running the filter, we need to find out what the
  // bounds of the data are in 3D space. To do this, first get the coordinate
  // system by calling GetCoordinateSystem on the DataSet returned in Step 2.
  // This method returns a vtkm::cont::CoordinateSystem object. Use the
  // GetBounds method of this returned object to get the physical bounds of the
  // data. The basic GetBounds method returns a vtkm::Bounds object.

  // Step 4
  // Create a vtkm::filter::PointElevation object. This object represents the
  // filter we will run on the data.

  // Step 5
  // Choose a name for the field we are creating with this filter. Set the
  // filter name with the SetOutputFieldName method. Names like "elevation" or
  // "height" are appropriate, but you can name it whatever you want ("foobar",
  // "Rocky Raccoon", "@oceans11").

  // Step 6
  // Set the low point and high point in physical space for the elevation using
  // the SetLowPoint and SetHighPoint methods. Each method takes three
  // parameters for the X, Y, Z coordinates. Use the Bounds object returned in
  // Step 3 to set the low and high points. For example, you could set the low
  // point to (bounds.X.Min, bounds.Y.Min, bounds.Z.Min) and the high point to
  // (bounds.X.Max, bounds.Y.Max, bounds.Z.Max). Also reasonable is setting the
  // low point to (0, bounds.Y.Min, 0) and the high point to
  // (0, bounds.Y.Max, 0)

  // Step 7
  // Run the PointElevation filter. The filter is run by calling the Execute
  // method. The Execute method for this filter takes two arguments: the input
  // DataSet (read in Step 2) and the coordinates to operate on (same as the
  // CoordinateSystem retrieved in Step 3). Execute returns a
  // vtkm::filter::ResultField object.

  // Step 8
  // Get the resulting data from the PointElevation filter. This is done by
  // calling GetDataSet on the ResultField returned in Step 7.

  // Now we are going to set up a rendering system to draw this data.

  // Step 9
  // Create a canvas object (representing the rendering system and drawing
  // area) of type vtkm::rendering::CanvasRayTracer and a mapper object
  // (representing the algorithm used to draw the data) of type
  // vtkm::rendering::MapperRayTracer. There are other implementations of
  // canvases/mappers that can use graphics hardware systems, but these
  // are the most portable.

  // Step 10
  // Create a vtkm::rendering::Actor object. The Actor constructor takes three
  // arguments: a cell set, a coordinate system, and a scalar field (to color
  // by). Data for all three arguments can be retrieved from the DataSet
  // retrieved in Step 8. The first argument by calling the GetCellSet method.
  // The second argument by calling the GetCoordinateSystem method. The third
  // argument by calling the GetField method with the name chosen in Step 5.

  // Step 11
  // Create a scene. First construct an object of type vtkm::rendering::Scene.
  // Then call the AddActor method giving it the actor created in Step 10.
  // (More complex scenes can contain multiple actors.)

  // Step 12
  // Create a vtkm::rendering::View3D object. This constructor takes three
  // arguments: the scene (created in Step 11), the mapper (created in Step 9),
  // and the canvas (created in Step 9).

  // Step 13
  // Call the Initialize method on the View3D object created in Step 12.

  // Step 14
  // Use the view to render the data. Do this by calling Paint on the View3D
  // object created in Step 12.

  // Step 15
  // Save the rendered image. The image is in portable pixel map format, which
  // should have a .ppm extension.
}

int main(int argc, char **argv)
{
  // Get the input filename from the arguments.
  if (argc < 2)
  {
    std::cerr << "USAGE: " << argv[0] << " <filename>.vtk" << std::endl;
    return 1;
  }
  const char *filename = argv[1];
  std::cout << "Loading file " << filename << std::endl;

  // It is good practice to wrap VTK-m calls in a try-catch block to check
  // for errors.
  try
  {
    Exercise(filename);
  }
  catch (vtkm::cont::Error &error)
  {
    std::cerr << "Encountered VTK-m error:" << std::endl;
    std::cerr << error.GetMessage() << std::endl;
  }

  return 0;
}
